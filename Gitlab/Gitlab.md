# GitLab


* ###  *[Regresar al menu](https://gitlab.com/dac.alvarez/capacitaciones-yv-mlab-2021/-/blob/master/README.md)*.
* ###  *[Ir a Git](https://gitlab.com/dac.alvarez/capacitaciones-yv-mlab-2021/-/blob/master/Git/Git.md)*.
* ### *[Biografia](https://gitlab.com/dac.alvarez/capacitaciones-yv-mlab-2021/-/blob/master/biografia/biografia.md)*.

1. [como-crear-un-repositorio](##como-crear-un-repositorio)
2. [Crear-Grupos](##Crear-Grupos)
3. [Sub-grupos](###New-Subgroup)
4. [Crear-Issues](##Crear-Issues)
---

GitLab es como GitHub pero en esteroides. Es un servicio que también ofrece alojamiento de repositorios con varias funciones de seguimientos de problemas, pero además tiene características extra.

GitLab fue fundado en 2011, y como cuentan ellos mismos, inició como un proyecto en GitHub. A diferencia de este último, en GitLab usan una aplicación única creada desde cero para dar soporte al ciclo entero de desarrollo, en lugar de integrar múltiples herramientas diferentes.
![foto](gitlab.png)

## como-crear-un-repositorio
* Configurar Git
Una vez que tenemos instalado git es hora de configurarlo. Para hacer esto necesitamos abrir el terminal y introducir lo siguiente:
git config --global user.name "DILAN ALEXANDER ALVAREZ CHAVEZ"
git config --global user.email "dac.alvarez@yavirac.edu.ec"
* Crea un nuevo repositorio
git clone git@gitlab.com:dac.alvarez/capacitaciones-yv-mlab-2021.git
cd capacitaciones-yv-mlab-2021
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

## Crear-Grupos

Un grupo de GitLab es un conjunto de proyectos, junto con los datos acerca de los usuarios que tienen acceso. Cada grupo tiene también un espacio de nombres específico (al igual que los usuarios).

* Se selecciona la opcion de Groups y en create group
![foto](groups.png)

* se desplegara la pantalla y sellenara los datos del nuevo grupo

![foto](creando.png)

* saldra un mensaje de que el grupo se creo exitosamente  

![foto](success.png)

* aqui tambien se puede crear sub grupos en la opcion de 
### New-Subgroup

![foto](success.png)

* Y aqui tamvien se creara los datos del sub grupo
![foto](subgrupo.png)

* y asi se veria los grupos creados

![foto](creadogrupo.png)

## Crear-Issues

Los problemas son el mecanismo fundamental en GitLab para colaborar en ideas, resolver problemas y planificar el trabajo.
Mediante los problemas, puede compartir y discutir propuestas (tanto antes como durante su implementación) entre usted y su equipo, y colaboradores externos.

Puede utilizar los problemas para muchos propósitos, personalizados según sus necesidades y flujo de trabajo. Los casos de uso comunes incluyen:

* Discutir la implementación de una nueva idea.
* Seguimiento de tareas y estado laboral.
* Aceptar propuestas de funciones, preguntas solicitudes de soporte o informes de errores.
* Elaborar nuevas implementaciones de código.

![foto](ejemissue.png)


* se selecciona la opcion de issue i se ingresa en el board
![foto](issue1.png)

* en una de las listas se da a la opcion de + y se pone el nombre al issue y en la opcion create

![foto](issue2.png)

* y se crea en los boards los issues
  

![foto](issuecreado.png)


