# Git

* ###  *[Regresar al menu](https://gitlab.com/dac.alvarez/capacitaciones-yv-mlab-2021/-/blob/master/README.md)*.
* ###  *[Ir a Gitlab](https://gitlab.com/dac.alvarez/capacitaciones-yv-mlab-2021/-/blob/master/Gitlab/Gitlab.md)*.
* ### *[Biografia](https://gitlab.com/dac.alvarez/capacitaciones-yv-mlab-2021/-/blob/master/biografia/biografia.md)*.

---
1. [¿Qué-es?](##¿Qué-es?)

2. [comandos](##comandos)

3. [GIT-Clientes](##GIT-Clientes)

4. [GitKraken:](###GitKraken:)
5. [Clonacion-de-proyectos-de-git](##Clonacion-de-proyectos-de-git)
*  [Por-Comando:](###Por-Comando)
*  [Por-Cliente-Kraken](###Por-Cliente-Kraken)

6. [commits-por-consola-y-por-Kraken](##commits-por-consola-y-por-Kraken)

* [Por-consola:](###Por-consola)
* [Por-Kraken](###Por-Kraken)
7. [Ramas-desde-Kraken](##Ramas-desde-Kraken)


## ¿Qué-es?
---
Es un sistema de control de versiones, es decir, un software que permite registrar todo el historial de cambios de un proyecto. Git es una herramienta que realiza una función del control de versiones de código de forma distribuida, de la que destacamos varias características:
•	Es muy potente
•	Fue diseñada por Linus Torvalds
•	No depende de un repositorio central
•	Es software libre
•	Con ella podemos mantener un historial completo de versiones
•	Podemos movernos, como si tuviéramos un puntero en el tiempo, por todas las revisiones de código y desplazarnos una manera muy ágil.
•	Es muy rápida
•	Tiene un sistema de trabajo con ramas que lo hace especialmente potente
•	En cuanto a la funcionalidad de las ramas, las mismas están destinadas a provocar proyectos divergentes de un proyecto principal, para hacer experimentos o para probar nuevas funcionalidades.
•	Las ramas pueden tener una línea de progreso diferente de la rama principal donde está el Core de nuestro desarrollo. En algún momento podemos llegar a probar algunas de esas mejoras o cambios en el código y hacer una fusión a nuestro proyecto principal, ya que todo esto lo maneja Git de una forma muy eficiente


## comandos
---
* git help

Muestra una lista con los comandos más utilizados en GIT.

* git init

Podemos ejecutar ese comando para crear localmente un repositorio con GIT y así utilizar todo el funcionamiento que GIT ofrece.  Basta con estar ubicados dentro de la carpeta donde tenemos nuestro proyecto y ejecutar el comando.  Cuando agreguemos archivos y un commit, se va a crear el branch master por defecto.

* git add + path

Agrega al repositorio los archivos que indiquemos.

* git add -A

Agrega al repositorio TODOS los archivos y carpetas que estén en nuestro proyecto, los cuales GIT no está siguiendo.

* git commit -m "mensaje" + archivos

Hace commit a los archivos que indiquemos, de esta manera quedan guardados nuestras modificaciones.

* git commit -am "mensaje"

Hace commit de los archivos que han sido modificados y GIT los está siguiendo.

* git checkout -b NombreDeBranch

Crea un nuevo branch y automaticamente GIT se cambia al branch creado, clonando el branch desde donde ejecutamos el comando.

* git branch

Nos muestra una lista de los branches que existen en nuestro repositorio.

* git checkout NombreDeBranch

Sirve para moverse entre branches, en este caso vamos al branch que indicamos en el comando.

* git merge NombreDeBranch

Hace un merge entre dos branches, en este caso la dirección del merge sería entre el branch que indiquemos en el comando, y el branch donde estémos ubicados.

* git status

Nos indica el estado del repositorio, por ejemplo cuales están modificados, cuales no están siendo seguidos por GIT, entre otras características.

* git clone URL/name.git NombreProyecto

Clona un proyecto de git en la carpeta NombreProyecto.

* git push origin NombreDeBranch

Luego de que hicimos un git commit, si estamos trabajando remotamente, este comando va a subir los archivos al repositorio remoto, específicamente al branch que indiquemos.

* git pull origin NombreDeBranch

Hace una actualización en nuestro branch local, desde un branch remoto que indicamos en el comando

## GIT-Clientes
---
Git viene con herramientas de GUI integradas para confirmar ( git-gui ) y navegar ( gitk ), pero hay varias herramientas de terceros para los usuarios que buscan una experiencia específica de la plataforma.

### GitKraken:
Si cuentan con algunos desarrollos y son usuarios de servicios como GitHub, gitlab, bitbucket o VSTS el día de hoy hablaremos de GitKraken una excelente herramienta que puede ser de su interés.

GitKraken es utilizado por empresas como Netflix, Tesla y Apple, es la base para los desarrolladores que están en busca de una interfaz más amigable para Git, con integraciones a GitHub, gitlab, bitbucket y VSTS (Azure DevOps).


Si eres un usuario de Git en Linux, es posible que estés utilizando la línea de comandos para interactuar con el servicio.

Sin embargo, si pudieran tener una herramienta GUI destacada para ese propósito, probablemente la usarían para hacer su trabajo un poco más eficiente.

Con Gitkraken puedes interactuar fácilmente con tu cuenta de Git, sin tener que usar la línea de comandos.

Gitkraken cuenta con versiones de paga y una versión gratuita en donde el cliente gratuito de Gitkraken pierde características como:

* Perfiles múltiples
* Potente editor de conflicto de fusión
* Integración GitHub Enterprise
* Integración de la comunidad GitLab

## Clonacion-de-proyectos-de-git
---
* ### Por-Comando
1. En Gitlab, visita la página principal del repositorio.
2. Sobre la lista de archivos, da clic en  Código.
3. Para clonar el repositorio usando HTTPS, en "Clonar con HTTPS", haga clic en. Para clonar el repositorio con una clave SSH, incluido un certificado emitido por la autoridad de certificación SSH de su organización, haga clic en Usar SSH y luego copie el link del proyecto 
4. Abre la Git Bash.
5. Cambia el directorio de trabajo actual a la ubicación en donde quieres clonar el directorio.
6. Escribe **git clone**, y luego pega la URL que copiaste antes.
7. $ git clone git@gitlab.com:dac.alvarez/Gitdocumentos.git
8. Presiona Enter para crear tu clon local.
9. $ git clone git@gitlab.com:dac.alvarez/Gitdocumentos.git
### Por-Cliente-Kraken
![foto](Kraken.png)
1. En Gitlab, visita la página principal del repositorio.
2. Sobre la lista de archivos, da clic en  Código.
3. Para clonar el repositorio usando HTTPS, en "Clonar con HTTPS", haga clic en. Para clonar el repositorio con una clave SSH, incluido un certificado emitido por la autoridad de certificación SSH de su organización, haga clic en Usar SSH y luego copie el link del proyecto 
4. ingresar a la aplicacion
5. seleccionar **Open a repo**
6. selecciona la opcion de clone 
7. se pega la direccion del pryecto 
8. y se da clic a clone to repo
![foto](clonar.png)

## commits-por-consola-y-por-Kraken
---
### Por-consola
creo el repositorio Git con el comando "git init".

git init

Una vez has inicializado el repositorio, podrás observar que se ha creado una carpeta en tu proyecto llamada ".git" . Si ves esa carpeta en tu proyecto es que el repositorio se ha inicializado correctamente y que ya tienes convertida esa carpeta en un repositorio de software Git.

1) Tengo que añadir el fichero a una zona intermedia temporal que se llama "Zona de Index" o "Staging Area" que es una zona que utiliza Git donde se van guardando los ficheros que posteriormente luego se van a hacer un commit.

Cualquier archivo que quieras mandar a la zona de index lo haces con el comando "add".

git add nombre-del-fichero

Una vez añadido podrías ver que realmente tienes ese fichero en el "staging area", mediante el comando "status".

git status

Verás que, entre las cosas que te aparecen como salida de ese comando, te dice que tienes tu fichero añadido como "new file". Además te dice que estos cambios están preparados para hacer commit.

2) Tengo que enviar los archivos de la zona de Index al repositorio, lo que se denomina el commit propiamente dicho. Lo haces con el siguiente comando:

git commit -m "mensaje para el commit"

### Por-Kraken

1. Se da la al boton de undo para poder hacer el commit
![foto](commit.png)
2. y en la parte inferior derecla solo se selecciona el boton **Commit Changes** para subir los archivos
![foto](commitchanges.png)

## Ramas-desde-Kraken
---
Para crear una branch en Git Kraken se debe seleccionar el commit desde el cual iniciará, ya que se copiarán todos los archivos existentes y su estado en dicha versión. Para ello se hace click izquierdo sobre la representación de éste en la parte central de la ventana de trabajo.

Luego, desplegar el menú del click derecho del mouse y seleccionar Create branch here.
![foto](Createbranchhere.png)


O bien, seleccionar el botón Branch en el menú horizontal superior del área de trabajo.
![foto](1cr.png)

Se desplegará una casilla donde debe escribir el nombre para su rama, finalizando el proceso con la tecla Enter.

![foto](2cr.png)

Ahora podrá visualizar su nueva branch en la área central, en un rectángulo con el nombre seleccionado y un icono de un computador, además de aparecer listado en los repositorios locales que se muestran en el área izquierda del sector de trabajo.

![foto](3cr.png)

Sin embargo, la rama sólo se encuentra en su repositorio local; en el repositorio remoto aún no se genera dicha rama, por lo que es necesario indicar explicitamente la generación de ésta.

Para ello, se abre un menú haciendo click derecho sobre el nombre del branch, ya sea en la sección izquierda o en la parte central del área de trabajo.
![foto](setup.png)

Se acepta la confirmación de la labor presionando el botón Submit.

![foto](Rama2.png)

